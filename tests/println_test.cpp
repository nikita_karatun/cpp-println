#include "gtest/gtest.h"

#include <iostream>
#include <string>

#include "cpp-println/println.hpp"

using namespace Println;

template <typename T>
std::string to_string(const T& value) {
    std::stringstream stream;
    print(stream, value);
    return stream.str();
}

template <typename T>
std::string to_string_with_new_line(const T& value) {
    std::stringstream stream;
    println(stream, value);
    return stream.str();
}

struct Foo {
    std::string value;
};

std::ostream& operator<<(std::ostream& stream, const Foo& foo) {
    return stream << "{value: " << foo.value << "}";
}

bool operator<(const Foo& o1, const Foo& o2) {
    return o1.value < o2.value;
}

auto new_line_char_string = ([]() {
    std::stringstream stream;
    stream << std::endl;
    return stream.str();
})();

TEST(print, should_print) {
    ASSERT_EQ("1111", to_string(1111));
    ASSERT_EQ("[1, 2, 3]", to_string(std::vector<int>({1, 2, 3})));
    ASSERT_EQ("[{1: 2}, {2: 3}]",
              to_string(std::map<int, int>({{1, 2}, {2, 3}})));
    ASSERT_EQ("[{{value: 1}: {value: 2}}, {{value: 2}: {value: 3}}]",
              to_string(std::map<Foo, Foo>({{{value : "1"}, {value : "2"}},
                                            {{value : "2"}, {value : "3"}}})));
}

TEST(print, should_print_with_new_line) {
    ASSERT_EQ("1111" + new_line_char_string, to_string_with_new_line(1111));
    ASSERT_EQ("[1, 2, 3]" + new_line_char_string,
              to_string_with_new_line(std::vector<int>({1, 2, 3})));
    ASSERT_EQ("[{1: 2}, {2: 3}]" + new_line_char_string,
              to_string_with_new_line(std::map<int, int>({{1, 2}, {2, 3}})));
    ASSERT_EQ(
        "[{{value: 1}: {value: 2}}, {{value: 2}: {value: 3}}]" +
            new_line_char_string,
        to_string_with_new_line(std::map<Foo, Foo>(
            {{{value : "1"}, {value : "2"}}, {{value : "2"}, {value : "3"}}})));
}