#include "gtest/gtest.h"

#include <list>
#include <map>

#include "cpp-println/println.hpp"

using namespace Println;

TEST(Is_iterable, should_check_if_type_is_iterable) {
    ASSERT_TRUE(Is_iterator_v<decltype(std::vector<int>().begin())>);
    ASSERT_TRUE(Is_iterator_v<std::vector<int>::iterator>);
    ASSERT_TRUE(Is_iterator_v<std::list<int>::iterator>);
    ASSERT_TRUE((Is_iterator_v<std::map<int, int>::iterator>));
    ASSERT_FALSE(Is_iterator_v<std::string>);
    ASSERT_FALSE(Is_iterator_v<int>);

    ASSERT_FALSE(Is_iterable_v<int>);
    ASSERT_TRUE(Is_iterable_v<std::vector<int>>);
}