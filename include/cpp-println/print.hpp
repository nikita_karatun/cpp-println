#pragma once

#include <iostream>
#include <iterator>
#include <type_traits>

namespace Println {

template <typename T, typename B = void>
struct Is_iterator : std::integral_constant<bool, false> {};

template <typename T>
struct Is_iterator<
    T, std::enable_if_t<!std::is_same<
           typename std::iterator_traits<T>::value_type, void>::value>>
    : std::integral_constant<bool, true> {};

template <typename T, typename B = void>
inline constexpr bool Is_iterator_v = Is_iterator<T, B>::value;

template <typename T, typename B = void>
struct Is_iterable : std::integral_constant<bool, false> {};

template <typename T>
struct Is_iterable<
    T, std::enable_if_t<Is_iterator_v<decltype(std::declval<T>().cbegin())> &&
                        Is_iterator_v<decltype(std::declval<T>().cend())>>>
    : std::integral_constant<bool, true> {};

template <typename T>
inline constexpr bool Is_iterable_v = Is_iterable<T>::value;

template <typename T, typename B = void>
struct Printer {
    static void print(std::ostream& stream, const T& value) {
        stream << value;
    }
};

template <typename T>
struct Printer<T, std::enable_if_t<std::is_fundamental_v<T>>> {
    static void print(std::ostream& stream, T value) {
        stream << value;
    }
};

template <typename K, typename V>
struct Printer<std::pair<K, V>> {
    static void print(std::ostream& stream, const std::pair<K, V>& value) {
        stream << "{" << value.first << ": " << value.second << "}";
    }
};

template <typename T>
struct Printer<T, std::enable_if_t<Is_iterable_v<T>>> {
    static void print(std::ostream& stream, const T& value) {
        stream << "[";
        auto it = value.cbegin();
        if (it != value.cend()) {
            using entry_type =
                std::remove_const_t<std::remove_reference_t<decltype(*it)>>;

            Printer<entry_type>::print(stream, *it);
            for (++it; it != value.cend(); ++it) {
                stream << ", ";
                Printer<entry_type>::print(stream, *it);
            }
        }
        stream << "]";
    }
};

template <typename T>
void print(std::ostream& stream, const T& value) {
    Printer<T>::print(stream, value);
}

template <typename T>
void print(const T& value) {
    Printer<T>::print(std::cout, value);
}

};  // namespace Println