#pragma once

#include "cpp-println/print.hpp"

namespace Println {

template <typename T>
void println(std::ostream& stream, const T& value) {
    Printer<T>::print(stream, value);
    stream << std::endl;
}

template <typename T>
void println(const T& value) {
    println(std::cout, value);
}

}  // namespace Println