# cpp-println

Tiny convinience header only library providing generalized print and println functions.

Following types supported:

* objects with defined `<<` operator,
* `std::pair`s of such objects,
* collections with `cbegin()` and `cend()` iterators pair.

# Usage

```c++

// src/usage.cpp

#include "cpp-println/println.hpp"

using namespace Println;

int main() {
    println(1);
    println("1");
    println(std::vector<int>({1, 2, 3}));
    println(std::map<int, int>({{1, 2}, {2, 3}}));
    println(std::list<int>({1, 2, 3}));
}

```

Output:

```bash

1
1
[1, 2, 3]
[{1: 2}, {2: 3}]
[1, 2, 3]

```