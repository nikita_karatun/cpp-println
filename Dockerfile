FROM debian:stable

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y g++ cmake wget bzip2 libgtest-dev \
    && rm -rf /var/lib/apt/lists/*