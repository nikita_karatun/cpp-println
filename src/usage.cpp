#include <vector>
#include <list>
#include <map>

#include "cpp-println/println.hpp"

using namespace Println;

int main() {
    println(1);
    println("1");
    println(std::vector<int>({1, 2, 3}));
    println(std::map<int, int>({{1, 2}, {2, 3}}));
    println(std::list<int>({1, 2, 3}));
}